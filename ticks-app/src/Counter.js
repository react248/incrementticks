import { Component } from "react";

export class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ticks: 0,
    };
  }

  incrementTicks = () => {
    this.setState({ ticks: this.state.ticks + 1 });
  };

  render() {
    return <button onClick={this.incrementTicks}>{this.state.ticks}</button>;
  }
}
